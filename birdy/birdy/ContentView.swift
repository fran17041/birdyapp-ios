//
//  ContentView.swift
//  birdy
//
//  Created by student on 13.12.2023..
//

import SwiftUI

struct Tweet{
    let username : String
    var content : String
    let date : Date
}

struct ContentView: View {
    var tweet : Tweet = Tweet(username: "fran", content: "volim ieee", date: Date())
    
    var body: some View {
            VStack{
                HStack{
                    Text("Birdy")
                        .font(.title)
                        .padding()
                    
                    Spacer()
                    Button(action:{})
                    {
                        Text("Login")
                    }
                    .padding()
                }
            
                HStack{
                    
                    Image("bfb")
                        .resizable()
                        .frame(width: 55, height: 55)
                        .clipShape(Circle())
                        .padding(.leading)
                    Spacer()
                    VStack{
                        Text(tweet.username)
                            .font(.title2)
                        Text(tweet.content)
                        Text(tweet.date, style:.relative)
                    }
                    .padding(.trailing)
                    
                }
                .border(Color.blue, width: 3)
                Spacer()
                
        }
    }
}

#Preview {
    ContentView()
}
